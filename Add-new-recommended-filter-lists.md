To make use of new recommended subscriptions in the UI, we first need to have them available in core (`adblockpluscore/data/subscriptions.json`). If the subscriptions are not listed there, follow the next steps:
  * Have the list hosted in our servers (easylist-downloads.adblockplus.org):
    * If the source is hosted in a public repository, ask [HelpDesk](https://jira.eyeo.com/projects/HD/summary)  to mirror the list.
    * Else, ask [Filters](https://jira.eyeo.com/projects/FT/summary) to create a [combination list](https://hg.adblockplus.org/easylistcombinations) from the list source we want to mirror.
  * Decide the `type` value for the new subscription list, that will be used as identifier in the UI (`subscription.recommended`).
  * Ask _Websites_ to support the new subscription `type` in both their [web.adblockplus.org](https://gitlab.com/eyeo/websites/web.adblockplus.org) repo (see [example commit](https://gitlab.com/eyeo/websites/web.adblockplus.org/-/commit/7b37c6028a77697fdbb4e90a1031a1562edd7f06)) and their [sitescripts](https://gitlab.com/eyeo/websites/sitescripts) repo (see [example ticket](https://gitlab.com/eyeo/websites/sitescripts/-/issues/2)).
  * Ask [Filters](https://jira.eyeo.com/projects/FT/summary) to add `type` value and the `[recommendation]` flag (see [example ticket](https://jira.eyeo.com/browse/FT-1587)).
  * Ask _Core_ to run `npm run update-subscriptions` to have the subscriptions available (see [ticket](https://gitlab.com/eyeo/adblockplus/adblockpluscore/-/issues/248)).
