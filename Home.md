Welcome to the wiki for the Adblock Plus UI project!

**Contents**

[[_TOC_]]

## Introduction

- [About Adblock Plus UI](https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/blob/main/README.md)

## Development

- [Local Development](local-dev)
- [Issue Lifecycle](issue-lifecycle)
- [Development Workflow](development-workflow)
- [Definition of Ready](Definition-of-Ready)
- [Definition of Done](definition-done)
- [GitLab Best Practises](gitlab-best-practises)

## Releases

- [Release Workflow](release-workflow)
  - also see [Development workflow](development-workflow)
  - also see [Translation workflow](translation-workflow)
- [Updates Page for Releases](Updates-page-for-releases)
- [Release History](releases)

## i18n

- [Core Languages for Translation](Core-Languages-for-Translation)
- [Translation Workflow](translation-workflow)
- [Translation Utilities](Utilities)

## Other

- [Utilities](Utilities)
- [Asset Optimization](Asset-Optimization)
- [Donation Campaigns](donation-campaigns)
- [Add new recommended filter lists](Add-new-recommended-filter-lists)
- [Setup filter server to test notifications](Setup-filter-server-to-test-notifications)
- [IO Custom Elements](IO-Custom-Elements)
