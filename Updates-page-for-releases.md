## Testing
A testing (mock) environment can be used to visualize the current state of the updates page:

Bundle the page with `npm run $ bundle.updates` and mount a local web server with `npm start`. This will allow you to access an index of HTML pages under the URL that `npm start` will show in your terminal (`http://localhost:8080` by default) and from there access `updates.html` page.

For changes made in the page logic (`updates.js`), style (`updates.scss`) and their dependencies (`landing.scss`, `io-components`, etc), you will need to bundle the page again (`npm run $ bundle.updates`) to visualize the changes.

## Modify: translation strings
File: `locale/en_US/updates-latest.json`

The following strings should correspond to each improvement / fix that would be listed in the updates page, where `[id]` is an identifier for each updates entry of the format `f1`, `f2`, ... for fixes and `i1`, `i2`, ... for improvements:
* `updates_update_[id]_description`: description
* `updates_update_[id]_title`: title
* `updates_update_[id]_image`: (optional) image [alternative text](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#attr-alt) description
* `updates_update_[id]_video`: (optional) video `aria-label` value used as text description for screen readers

`<strong>` and `<em>` tags are allowed to style the strings

## Media resources
Current directory: `skin/icons/updates/`

1. Remove outdated resources and add new ones.
2. Optimize the new assets running `npm run $ optimize.[type] [filePath]` where `[type]` can  take the values `gif`, `png`, `svg`, `mp4` and `[filePath]` is the asset path to optimize. Example: `npm run $ optimize.mp4 skin/icons/updates/video.mp4`


## Modify: updates.json
File: `data/updates.json`

File format:

- `title`: `<object>`
  - `image`: `<string>` URL for the hero image that will appear in the updates page header
- `fixes`/`improvements`: `<array>` a series of `<object>`s that will contain the following keys:
  - `id`: `<string>` the entry id; `f1`, `f2`, ... for fixes and `i1`, `i2`, ... for improvements.
  - `image`: `<object>` (optional) image that will be displayed as part of the updates entry
    - `url`: `<string>` image URL (e.g. `"skin/icons/updates/image.png"`)
  - `video`: `<object>` (optional) video that will be displayed as part of the updates entry
    - `url`: `<string>` video URL (e.g. `"skin/icons/updates/video.mp4"`)
    - `type`: `<string` the [MIME media video type](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Containers) (e.g. `"video/mp4"`)
  - `doclink`: `<string>` (optional) redirect parameter (`https://adblockplus.org/redirect?link=[doclink]`) for an entry's "Read more" link.

## Modify `updatesVersion`
File: `lib/prefs.js`

In order to trigger the `newtab` updates notification, we need to increase the value of `updatesVersion`.

The notification can't be tested with the testing (mock) environment and [building the extension](https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/#building-the-extension) would be required.

