# ABP UI development workflow

- [Workflow](#workflow)
- [Branches](#branches)
- [Commits](#commits)

## Workflow

![](res/development-workflow/abp-ui-gitlab-feature-development-workflow.svg)

- New features are developed in issue branches that are squash-merged into
  **Feature** branches (see Feature #1, #2 and #3). **Feature** branches contain
  all changes that are required for the feature to be complete
  (incl. bug fixes, translations).
- Completed **Feature** branches are merged into the **Next** branch.
- Upon _**Feature Freeze**_, the **Next** branch is merged into the **Release**
  branch. Any bugs found during testing are merged directly into the **Release**
  branch.
- Upon _**Code Freeze**_, the **Release** branch is merged into the **Main**
  branch. Any hot fixes (e.g. "Bug fix #1") that are required before or after
  the release it made can be merged directly into the **Main** branch.
- If changes to strings were made, they are merged into a feature branch, listed
  in the issue under "Hints for translators" and uploaded to XTM to request
  translations. When those are ready, they are imported into the feature branch.
  See also [translation workflow](translation-workflow).

## Branches

- [Feature](#feature)
- [Next](#next)
- [Release](#release)
- [Main](#main)

### Feature

These branches are meant for features that cannot be released as-is, because
they consist of multiple issues and/or require other changes to be done first,
such as translations.

### [Next][gitlab-next]

This branch is the main development branch and contains all features that are
ready to be released. It represents the latest development version of Adblock
Plus.

### [Release][gitlab-release]

This branch represents the release candidate using which release testing is
done.

### [Main][gitlab-main]

This branch is being mirrored to [github.com/adblockplus/adblockplusui][github].
It represents the latest production version of Adblock Plus.

## Commits

- [Commit message](#commit-message)
- [Removal commits](#removal-commits)
- [Noissue commits](#noissue-commits)

### Commit message

We are following the [Conventional Commits][convcomms] specification
with the format `type: Message [#123]`:
- `type`: Any of the [predefined types](#commit-types)
- `Message`: Commit message
- `#123`: GitLab issue number (use `noissue` for [Noissue commits](#noissue-commits))

Example: `build: Updated @eyeo/snippets dependency to 0.5.2 [#1214]`

#### Commit types

based on [Angular convention][convcomms-types]

| Type | Changes | Example scopes |
|-|-|-|
| build | Changes that affect the build system, external dependencies or development tools | GitLab, gulp, npm |
| ci | Changes to our CI configuration files and scripts | BrowserStack, GitLab CI |
| docs | Documentation only changes | README.md, code comments |
| feat | A new feature | - |
| fix | A bug fix | - |
| i18n | Importing translations | translation files |
| perf | A code change that improves performance | - |
| refactor | A code change that neither fixes a bug nor adds a feature | - |
| style | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) | - |
| test | Adding missing tests or correcting existing tests | - |

### Removal commits

Dedicated removal commits should be made when removing modules, components or
other significant code parts that we may need to refer back to later on
(e.g. for reusing parts of a removed component).

Those commits should ideally be done as part of the same issue that makes the
removed code redundant. Alternatively, a follow-up issue can be created for the
purpose of removing such code.

### Noissue commits

Noissue commits are code changes that don't have an issue associated with them.
They lack context and further information for other stakeholders
(i.e. integrators, testers, translators) and are not part of the regular
development flow (i.e. no Copy LGTM, Legal LGTM, Feature QA, etc.).

Noissue commit messages should include a link to the respective review,
if applicable.

Allowed for…
- Adding release tags (no review required)
- Backing out changes (no review required)
- Importing translations for multiple issues from Crowdin and XTM (review
  required)

Debatable for…
- Updating minor versions of dependencies
- Backporting changes from other branch
- Fixing linting errors

Not allowed for…
- Minor bug fixes
- Changing metadata files (e.g. .gitignore)
- Changing build-related code
- Changing translation files
- Removing unused files/code
- …and everything that's not explicitly allowed


[convcomms]: https://www.conventionalcommits.org/en/v1.0.0/
[convcomms-types]: https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#type
[github]: https://github.com/adblockplus/adblockplusui
[gitlab-main]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/tree/main
[gitlab-next]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/tree/next
[gitlab-release]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/tree/release
